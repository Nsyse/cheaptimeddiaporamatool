﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiapoTool
{
    public class SlideModel
    {
        public Image Image { get; set; }
        public string ImagePath { get; set; }

        public SlideModel(Image image, string filePath)
        {
            this.Image = image;
            this.ImagePath = filePath;
        }
    }
}
