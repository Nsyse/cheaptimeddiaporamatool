﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiapoTool
{
    public partial class DiapoUI : Form
    {
        private Rectangle resolution;
        private DiaporamaModel m_DiaporamaModel;
        private SettingsForm m_Master;

        private readonly int m_TitleBarHeight;

        public DiapoUI()
        {
            InitializeComponent();
        }

        public DiapoUI(Rectangle resolution, DiaporamaModel diaporamaModel, SettingsForm master)
        {
            m_Master = master;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            this.resolution = resolution;
            this.m_DiaporamaModel = diaporamaModel;

            Rectangle screenRectangle = RectangleToScreen(this.ClientRectangle);
            m_TitleBarHeight = screenRectangle.Top - this.Top;

            master.DiapoTimer.Tick += new EventHandler(TimerEventProcessor);
            master.DiapoTimer.Interval = m_DiaporamaModel.TimeDisplayEach * 1000;

            DisplayScaledSlide(m_DiaporamaModel.SlideList[0]);

        }
        private void TimerEventProcessor(Object myObject,
                                            EventArgs myEventArgs)
        {
            DisplayScaledSlide(m_DiaporamaModel.NextSlide());
            if (m_DiaporamaModel.m_SlideshowDone)
            {
                m_Master.PauseResume();
                m_DiaporamaModel.m_SlideshowDone = false;
            }
        }

        private void DisplayScaledSlide(SlideModel chosenSlide)
        {
            pictureBox1.Image = ScaleImage(chosenSlide.Image, resolution.Width, resolution.Height - (panel1.Size.Height + m_TitleBarHeight));
            pictureBox1.Location = new Point((pictureBox1.Parent.ClientSize.Width / 2) - (pictureBox1.Image.Width / 2),
                              0);
            pictureBox1.Refresh();
            label1.Text = "Image " + (m_DiaporamaModel.CurrentIndex + 1) + " out of " + m_DiaporamaModel.SlideList.Count;
            label2.Text = chosenSlide.ImagePath;
        }

        //https://stackoverflow.com/questions/6501797/resize-image-proportionally-with-maxheight-and-maxwidth-constraints
        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            m_Master.PauseResume();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_Master.PauseResume();
        }

        private void DiapoUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_Master.StopTimer();
            m_Master.DiapoTimer.Tick -= TimerEventProcessor;
            m_Master = null;
        }
    }
}
