﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiapoTool
{
    public class DiaporamaModel
    {
        private static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPE", ".BMP", ".GIF", ".PNG" };

        public int TimeDisplayEach { get; set; }

        private List<SlideModel> m_ImageList = new List<SlideModel>();
        public List<SlideModel> SlideList { get { return m_ImageList; }
            set { this.m_ImageList = value; } }

        public int CurrentIndex = 0;
        public bool m_SlideshowDone = false;

        public bool MustLoop { get; set; }

        private bool m_IsPlaying = false;
        public bool IsPlaying {
            get {return m_IsPlaying; }
            set { m_IsPlaying = value; }
        }

        internal void GenerateImageList(string[] files)
        {
            SlideList = new List<SlideModel>();
            string test = "";
            //For each file in files
            for (int i = 0; i < files.Length; i++)
            {
                //If its type matches a image
                if (ImageExtensions.Contains(Path.GetExtension(files[i]).ToUpperInvariant()))
                {
                    //Add it to the list of images
                    test = files[i];
                    SlideList.Add(new SlideModel(Image.FromFile(files[i]), files[i]));
                }
            }

            //If list of image is empty, show error message.
            if (SlideList.Count == 0)
            {
                MessageDialogUtils.ShowErrorMessageDialog("Invalid folder selected", "Invalid folder.\nThe folder must contain at least one image (jpg, jpe, bmp, gif or png)");
            }
            else
            {
                //MessageDialogUtils.ShowErrorMessageDialog("Valid folder", test);
            }
        }

        internal SlideModel NextSlide()
        {
            if (m_SlideshowDone)
            {
                CurrentIndex = SlideList.Count-1;
            }
            else
            {
                CurrentIndex++;
                if (CurrentIndex >= SlideList.Count)
                {
                    if (!MustLoop)
                    {
                        m_SlideshowDone = true;
                        MessageDialogUtils.ShowErrorMessageDialog("Completed!", "The slideshow is done!");
                    }
                    CurrentIndex = 0;
                }
            }
            return SlideList[CurrentIndex];
        }

        internal void Shuffle()
        {
            m_SlideshowDone = false;

            Random rnd = new Random();
            int numberOfSwaps = SlideList.Count* 10;
            SlideModel tempSlide;
            for (int i = 0; i < numberOfSwaps; i++)
            {
                int firstIndex = rnd.Next(0, SlideList.Count);
                int secondIndex = rnd.Next(0, SlideList.Count);
                tempSlide = SlideList[firstIndex];
                SlideList[firstIndex] = SlideList[secondIndex];
                SlideList[secondIndex] = tempSlide;
            }
        }
    }
}
