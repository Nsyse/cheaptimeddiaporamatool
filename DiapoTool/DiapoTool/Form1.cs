﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiapoTool
{
    public partial class SettingsForm : Form
    {

        private DiaporamaModel m_DiaporamaModel = new DiaporamaModel();

        public Timer DiapoTimer = new Timer();

        private DiapoUI diapoForm;

        public SettingsForm()
        {
            InitializeComponent();
        }


        //Select diaporama folder
        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    m_DiaporamaModel.GenerateImageList(files);

                    if (m_DiaporamaModel.SlideList.Count > 0)
                    {
                        button1.Text = fbd.SelectedPath;
                    }
                    else
                    {
                        button1.Text = "Select the folder to play the diaporama from";
                    }
                }
            }
        }

        //Start button
        private void button2_Click(object sender, EventArgs e)
        {
            UpdateImageTime();
            if (m_DiaporamaModel.TimeDisplayEach > 0 && m_DiaporamaModel.SlideList.Count > 0)
            {
                m_DiaporamaModel.MustLoop = checkBox1.Checked;
                CreateAndStartDiaporama();
            }
            else
            {
                string errorMessage = "Invalid Form Error.";
                if (m_DiaporamaModel.TimeDisplayEach <= 0)
                {
                    errorMessage += "\n-The time to display must be a numerical integer higher than 0";
                }
                if (m_DiaporamaModel.SlideList.Count <= 0)
                {
                    errorMessage += "\n-The selected folder is invalid. It should Contain at least one image." +
                        "\nAlternatively, it might simply not be set.";
                }

                MessageDialogUtils.ShowErrorMessageDialog("Invalid Form Error.", errorMessage);
            }
        }

        private void CreateAndStartDiaporama()
        {
            m_DiaporamaModel.Shuffle();
            Rectangle resolution = Screen.PrimaryScreen.Bounds;
            diapoForm = new DiapoUI(resolution, m_DiaporamaModel, this);
            diapoForm.Show();
            PauseResume();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateImageTime();
        }

        private void UpdateImageTime()
        {
            int secondsPerImage = 0;
            if (Int32.TryParse(textBox1.Text, out secondsPerImage))
            {
                m_DiaporamaModel.TimeDisplayEach = secondsPerImage;
            }
            else
            {
                m_DiaporamaModel.TimeDisplayEach = 0;
            }
        }

        public void PauseResume()
        {
            if (m_DiaporamaModel.IsPlaying)
            {
                //Pause
                //SetTitleBar to paused

                diapoForm.Text = "Slideshow tool - Stopped";
                DiapoTimer.Stop();
            }
            else
            {
                //Resume
                diapoForm.Text = "Slideshow tool - Playing";
                DiapoTimer.Start();
            }
            m_DiaporamaModel.IsPlaying = !m_DiaporamaModel.IsPlaying;
        }

        public void StopTimer()
        {
            DiapoTimer.Stop();
        }
    }
}
